# CIGeoE Toggle Vertex Visibility

This plugin toggle vertex visibility.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_toggle_vertex_visibility” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  ![ALT](./images/image01.png)

  3 - Select “CIGeoE Toggle Vertex Visibility”

  ![ALT](./images/image02.png)

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_toggle_vertex_visibility” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"



# Usage

1 - Select the feature to edit from the Layers Panel. 

![ALT](./images/image03.png)

2 - Activate the plugin by clicking on the plugin icon. All the vertices became visible.

![ALT](./images/image04.png)




# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
